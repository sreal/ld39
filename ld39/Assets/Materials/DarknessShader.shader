﻿Shader "Darkness" {
    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _AspectRatio ("Screen Aspect Ratio", Float) = 0
        _FadeAlpha ("Title Screen Fade In/Out", Float) = 0
        _OldWomanLight ("Old Woman Light", Vector) = (0, 0, 0, 0)
        _GoalTriggerLight ("Goal Trigger Light", Vector) = (0, 0, 0, 0)
    }
    SubShader {
        Pass {
            CGPROGRAM
            #pragma vertex vert_img
            #pragma fragment frag

            #include "UnityCG.cginc"

            uniform sampler2D _MainTex;

            uniform float _AspectRatio;
            uniform float _FadeAlpha;

            uniform float4 _OldWomanLight;
            uniform float4 _GoalTriggerLight;

            float wave(float x, float amount) {
                return (sin(x * amount) + 1.0) * 0.5;
            }

            float4 frag(v2f_img i) : COLOR {

                float4 c = tex2D(_MainTex, i.uv);
                float2 ratio = float2(1, 1/_AspectRatio);
                float delta = _FadeAlpha;

                float ray = length((_OldWomanLight.xy - i.uv.xy) * ratio);
                delta += smoothstep(_OldWomanLight.z, 0, ray) * _OldWomanLight.w;

                ray = length((_GoalTriggerLight.xy - i.uv.xy) * ratio);
                delta += smoothstep(_GoalTriggerLight.z, 0, ray) * _GoalTriggerLight.w;

                c.rgb *= delta;
                return c;
            }
            ENDCG
        }
    }
}
