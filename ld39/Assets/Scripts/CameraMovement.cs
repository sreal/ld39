using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public Transform sceneLocation;
    public Transform CameraStartLocation;
    float speed = .5f;

    void Awake() {
        transform.position = CameraStartLocation.position;
    }

    public void FadeUp() {
        StartCoroutine(TransitionCamera(sceneLocation.position, CameraStartLocation.position));
    }
    public void FadeDown() {
        StartCoroutine(TransitionCamera(CameraStartLocation.position, sceneLocation.position));
    }

    IEnumerator TransitionCamera(Vector3 from, Vector3 to)
    {
        var startTime = Time.time;
        while(transform.position != to)
        {
            var offset = (Time.time - startTime) * speed;
            transform.position = Vector3.Lerp(from, to, offset);
            yield return null;
        }
    }

    public float FadeAmount() {
        var end = sceneLocation.position.y;
        var start = CameraStartLocation.position.y;
        var distance = end - start;
        var current = transform.position.y - start;
        return current/distance;
    }
}
