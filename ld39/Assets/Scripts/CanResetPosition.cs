using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanResetPosition : MonoBehaviour {
    Vector3 originPosition;
    Quaternion originRotation;

    void Awake () {
        originPosition = transform.position;
        originRotation = transform.rotation;
    }

    public void ResetPosition() {
        transform.position = originPosition;
        transform.rotation = originRotation;
    }
}
