using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootstepAudio : MonoBehaviour {
    AudioSource footstep;
    void Awake () {
        footstep = GetComponent<AudioSource>();
    }
    public void Footstep() {
        var rndPitchChange = Random.value - 0.5f;
        footstep.pitch = 1 + rndPitchChange;
        footstep.Play();
    }

}
