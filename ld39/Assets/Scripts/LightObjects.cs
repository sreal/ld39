using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// https://boiagames.blogspot.com.au/2015/08/simplified-lighting-for-unity-2d-using.html?m=1
public class LightObjects : MonoBehaviour {

    public Material mat;
    public GameObject[] ToLight;

    void Awake () {
        mat.SetFloat("_AspectRatio", Camera.main.aspect);
    }

    void OnRenderImage(RenderTexture src, RenderTexture dest) {

        var fadeAlpha = 1 - Camera.main.GetComponent<CameraMovement>().FadeAmount();
        mat.SetFloat("_FadeAlpha", fadeAlpha);

        foreach (GameObject go in ToLight) {
            Vector2 position = go.transform.position;
            Vector4 worldPosition = Camera.main.WorldToViewportPoint(position);
            // Use z for radius in shader.
            worldPosition.z = 0.15f;
            worldPosition.w = 1;
            // allow access in shader
            mat.SetVector("_" + go.name + "Light", worldPosition);
        }
        Graphics.Blit(src, dest, mat);
    }

}
