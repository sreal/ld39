﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    bool inGame = false;

    void Update () {
        if (!inGame) {
            if (Input.GetButton("Fire1")) {
                StartGame();
            }
        }
    }

    void Reset() {
        Object.FindObjectOfType<CanResetPosition>().ResetPosition();
    }

    void StartGame() {
        Reset(); // Keep the OldWoman at the end on StopGame
        inGame = true;
        Camera.main.GetComponent<CameraMovement>().FadeDown();
        Object.FindObjectOfType<CanResetPosition>().ResetPosition();
    }

    public void StopGame() {
        inGame = false;
        Camera.main.GetComponent<CameraMovement>().FadeUp();
    }
}
