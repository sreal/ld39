﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    float SpeedMultiplier = 1.5f;
    float FallSpeedMultiplier = 1.5f;
    public Transform groundCheckStart, groundCheckEnd;
    public Transform stairCheckStart, stairCheckEnd;

    Animator animator;

    void Awake () {
        animator = GetComponent<Animator>();
    }

    void Update() {
        var horizontal = Input.GetAxis("Horizontal");

        var direction = 0;
        if (horizontal > 0) {
            if (transform.rotation.y != 0) {
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            direction = 1;
        } else if (horizontal < 0) {
            if (transform.rotation.y != 1) {  // Euler angle?
                transform.rotation = Quaternion.Euler(0, 180, 0);
            }
            direction = -1;
        }

        var x = horizontal * Time.deltaTime * SpeedMultiplier;
        transform.Translate(direction * x, 0, 0);
        animator.SetBool("Walking", x != 0);
        animator.SetFloat("Speed", horizontal);

        var vertical = Input.GetAxis("Vertical");
        RaycastHit2D ground = default(RaycastHit2D);
        if (vertical > -.2) {
            // Only cast if not trying to get through floor platform.
            ground = Physics2D.Linecast(groundCheckStart.position, groundCheckEnd.position, 1 << LayerMask.NameToLayer("Ground"));
        }
        RaycastHit2D stairs;
        stairs = Physics2D.Linecast(stairCheckStart.position, stairCheckEnd.position, 1 << LayerMask.NameToLayer("Stairs"));
        if (!stairs) {
            if (!ground) {
                transform.Translate(0, -Time.deltaTime * SpeedMultiplier * FallSpeedMultiplier, 0);
            } else {
                Debug.DrawLine(groundCheckStart.position, groundCheckEnd.position, Color.green);
                var groundCheckOffset = stairCheckEnd.InverseTransformPoint(ground.point).y;
                transform.Translate(0, groundCheckOffset, 0);
            }
        } else {
            Debug.DrawLine(stairCheckStart.position, stairCheckEnd.position, Color.green);
            var stepHeight = stairCheckEnd.InverseTransformPoint(stairs.point).y;
            transform.Translate(0, stepHeight, 0);
        }

    }
}
