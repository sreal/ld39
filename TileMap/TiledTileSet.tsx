<?xml version="1.0" encoding="UTF-8"?>
<tileset name="PyxelTileSet" tilewidth="32" tileheight="32" tilecount="12" columns="4">
 <image source="PyxelTileSet.png" width="128" height="96"/>
 <tile id="4">
  <objectgroup draworder="index"/>
 </tile>
 <tile id="5">
  <objectgroup draworder="index"/>
 </tile>
 <tile id="6">
  <objectgroup draworder="index"/>
 </tile>
 <tile id="7">
  <objectgroup draworder="index">
   <object id="7" x="0" y="0" width="32"/>
   <object id="8" x="0" y="0" height="32"/>
  </objectgroup>
 </tile>
 <tile id="9">
  <objectgroup draworder="index"/>
 </tile>
</tileset>
